<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Collections;

use ArrayAccess;
use InvalidArgumentException;

/**
 * Class BasicArray
 * @package Extended\Collections
 */
class BasicArray implements ArrayAccess, Reducable, Mappable, Filterable
{
    /**
     * @var array
     */
    protected $store;

    /**
     * MappableArray constructor.
     * @param array $arr
     */
    public function __construct(array $arr = [])
    {
        $this->store = $arr;
    }

    /**
     * @return array
     */
    public function __invoke()
    {
        return $this->store;
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->store);
    }

    /**
     * @param mixed $offset
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset)) {
            throw new InvalidArgumentException("Array key: '{$offset}' does not exist.");
        }

        return $this->store[$offset];
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     * @return $this
     * @throws InvalidArgumentException
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($this->store[$offset])) {
            $this->store[] = $value;
        } else {
            $this->store[$offset] = $value;
        }

        return $this;
    }

    /**
     * @param mixed $offset
     * @return $this
     * @throws InvalidArgumentException
     */
    public function offsetUnset($offset)
    {
        if (!$this->offsetExists($offset)) {
            throw new InvalidArgumentException("Array key: '{$offset}' does not exist.");
        }

        unset($this->store[$offset]);
        return $this;
    }

    /**
     * Mapping is immutable, creating a new buffer of array items with the
     * array callback applied to each top level value.
     *
     * @param callable $callback
     * @return array
     */
    public function map(callable $callback): array
    {
        $tmp = [];

        foreach ($this->store as $key => $value) {
            $tmp[$key] = $callback($value);
        }

        return $tmp;
    }

    /**
     * Reduced the internal array buffer based on the
     *
     * @param callable $callback
     * @return array
     */
    public function reduce(callable $callback): array
    {
        foreach ($this->store as $key => $value) {
            $value = $callback($value);
            if ($value) {
                $this->store[$key] = $value;
            } else {
                $this->offsetUnset($key);
            }
        }

        return $this->store;
    }

    /**
     * @param callable $sort
     * @return array
     */
    public function filter(callable $sort): array
    {
        $tmp = [];

        foreach ($this->store as $key => $value) {
            if ($sort($value, $key)) {
                $tmp[$key] = $value;
            }
        }

        return $tmp;
    }

    /**
     * @return array
     */
    public function keys(): array
    {
        return array_keys($this->store);
    }

    /**
     * @return array
     */
    public function values(): array
    {
        return array_values($this->store);
    }

    /**
     * @return mixed
     */
    public function random()
    {
        return array_rand($this->store);
    }

    /**
     * @return array
     */
    public function unique(): array
    {
        return array_unique($this->store);
    }
}