<?php

namespace Extended\Collections;

/**
 * Interface FactoryInterface
 * @package Extended\Collections
 */
interface FactoryInterface
{
    /**
     * @return mixed
     */
    public static function create();
}