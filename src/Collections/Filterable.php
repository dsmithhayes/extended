<?php

namespace Extended\Collections;

/**
 * Interface Filterable
 * @package Extended\Collections
 */
interface Filterable
{
    /**
     * The callable should be a boolean return
     *
     * @param callable $sort
     * @return array
     */
    public function filter(callable $sort): array;
}