<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Collections\Queue;

use Extended\Collections\Queue;
use Iterator;

/**
 * Class BasicQueueFactory
 * @package Extended
 */
class BasicQueue implements Queue, Iterator
{
    /**
     * @var array
     */
    protected $data;

    /**
     * BasicQueueFactory constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = array_values($data);
        $this->index = 0;
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->dequeue();
    }

    /**
     * @return int
     */
    public function key()
    {
        return 0;
    }

    /**
     *
     */
    public function next()
    {
        return null;
    }

    /**
     *
     */
    public function rewind()
    {
        return null;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return !is_null($this->data[0]);
    }

    /**
     * @param mixed $item
     * @return $this
     */
    public function enqueue($item)
    {
        $this->data[] = $item;
        return $this;
    }

    /**
     * @return mixed
     */
    public function dequeue()
    {
        return array_shift($this->data);
    }
}