<?php

namespace Extended\Collections\Queue;

use Extended\Collections\Queue\BasicQueue;
use Extended\Collections\FactoryInterface;
use InvalidArgumentException;

/**
 * Class BasicQueueFactory
 * @package Extended\Collections\Queue
 */
class BasicQueueFactory implements FactoryInterface
{
    /**
     * If there is no value passed to the Factory, create an empty BasicQueue.
     *
     * @param array $config
     * @return \Extended\Collections\Queue\BasicQueue
     */
    public static function create($config = [])
    {
        return new BasicQueue($config);
    }
}