<?php

namespace Extended\Collections\Queue;

use Generator;

/**
 * Class GeneratorQueue
 * @package Extended\Collections\Queue
 */
class GeneratorQueue extends BasicQueue
{
    /**
     * @return Generator
     */
    public function dequeue()
    {
        yield parent::dequeue();
    }
}