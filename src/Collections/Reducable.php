<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Collections;

/**
 * An Reducable array will apply the callback to its member values.
 *
 * Interface Reducable
 * @package Extended\Collections
 */
interface Reducable
{
    /**
     * @param callable $callback
     * @return array
     */
    public function reduce(callable $callback): array;
}