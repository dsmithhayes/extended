<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Collections\Stack;

use Extended\Collections\Stack;
use Extended\Exception\StackException;
use Iterator;

/**
 * The BasicStack object represents a very slim implementation of a stack.
 *
 * Class BasicStack
 * @package Extended
 */
class BasicStack implements Stack, Iterator
{
    /**
     * @var array
     *      The stack
     */
    protected $stack;

    /**
     * @var int
     *      The stack pointer
     */
    protected $pointer;

    /**
     * @param array $stack
     *      A stack to use within the object
     */
    public function __construct($stack = [])
    {
        $this->stack = array_values($stack);
        $this->pointer = (count($this->stack) - 1);
    }

    /**
     * @return mixed|null
     */
    public function pop()
    {
        $this->pointer--;
        $item = array_pop($this->stack);
        return $item;
    }

    /**
     * @param mixed $value
     *      The item to add to the stack
     */
    public function push($value)
    {
        $this->stack[++$this->pointer] = $value;
    }

    /**
     * @return mixed
     *      The current item in the stack
     */
    public function current()
    {
        return $this->pop();
    }

    /**
     * @return int
     *      The current position of the stack pointer
     */
    public function key(): int
    {
        return $this->pointer;
    }

    /**
     * Decrements the stack pointer to the next value in the list.
     *
     * @return void
     */
    public function next()
    {
        $this->pointer--;
    }

    /**
     * Resets the stack pointer to the top of the stack.
     *
     * @return void
     */
    public function rewind()
    {
        $this->pointer = count($this->stack) - 1;
    }

    /**
     * Checks if the stack pointer points to a valid location in the stack
     *
     * @return bool
     *      True if the item exists in the stack
     */
    public function valid(): bool
    {
        return !is_null($this->stack[$this->pointer]);
    }

    /**
     * Assures the stack pointer can't be set to an invalid range
     *
     * @param  int $value
     *      A stack pointer to set
     * @return int
     *      A valid stack pointer
     */
    protected function pointerCeiling(int $value): int
    {
        $ceiling = count($this->stack) - 1;
        return ($value > $ceiling) ? $ceiling : $value;
    }

    /**
     * Assures the stack pointer can't be set to an invalid range
     *
     * @param  int $value
     *      A stack pointer to set
     * @return int
     *      A valid stack pointer
     */
    protected function pointerFloor(int $value): int
    {
        return ($value < 0) ? 0 : $value;
    }
}
