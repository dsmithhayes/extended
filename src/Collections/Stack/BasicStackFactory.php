<?php

namespace Extended\Collections\Stack;

use Extended\Collections\Stack\BasicStack;
use Extended\Collections\FactoryInterface;
use InvalidArgumentException;

/**
 * Class BasicStackFactory
 * @package Extended\Collections\Stack
 */
class BasicStackFactory implements FactoryInterface
{
    /**
     * @param array $items
     * @return \Extended\Collections\Stack\BasicStack|mixed
     */
    public static function create(array $items = [])
    {
        return new BasicStack($items);
    }
}