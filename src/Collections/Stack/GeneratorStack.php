<?php

namespace Extended\Collections\Stack;

/**
 * Class GeneratorStack
 * @package Extended\Collections\Stack
 */
class GeneratorStack extends BasicStack
{
    /**
     * @return \Generator|mixed|null
     */
    public function pop()
    {
        yield parent::pop();
    }
}