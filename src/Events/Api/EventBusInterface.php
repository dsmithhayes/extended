<?php

namespace Extended\Events\Api;

use Extended\Events\Api\EventInterface;

/**
 * Interface EventBusInterface
 * @package Extended\Events\Api
 */
interface EventBusInterface
{
    /**
     * @param \Extended\Events\Api\EventInterface $event
     * @return mixed
     */
    public function addEvent(EventInterface $event);
}