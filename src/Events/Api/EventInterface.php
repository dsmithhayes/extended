<?php

namespace Extended\Events\Api;

/**
 * Interface EventInterface
 * @package Extended\Events\Api
 */
interface EventInterface
{
    /**
     * @return mixed
     */
    public function execute();

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return mixed
     */
    public function setName(string $name);
}