<?php

namespace Extended\Events;

use Extended\Collections\Queue;
use Extended\Collections\Queue\BasicQueue;
use Extended\Events\Api\EventBusInterface;
use Extended\Events\Api\EventInterface;
use Generator;

/**
 * Class EventQueue
 * @package Extended\Events
 */
class EventQueue implements EventBusInterface
{
    /**
     * @var BasicQueue
     */
    protected $_eventQueue;

    /**
     * EventQueue constructor.
     *
     * @param array|EventInterface[] $events
     */
    public function __construct(array $events = [])
    {
        $this->_eventQueue = new BasicQueue();

        if (count($events)) {
            foreach ($events as $event) {
                $this->addEvent($event);
            }
        }
    }

    /**
     * @param EventInterface $event
     * @return $this
     */
    public function addEvent(EventInterface $event)
    {
        $this->_eventQueue->enqueue($event);
        return $this;
    }

    /**
     * @return EventInterface
     */
    public function nextEvent(): EventInterface
    {
        return $this->_eventQueue->dequeue();
    }

    /**
     * @return Generator
     */
    public function all(): Generator
    {
        foreach ($this->_eventQueue->dequeue() as $event) {
            yield $event;
        }
    }
}