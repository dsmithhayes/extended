<?php

namespace Extended\Events;

use Extended\Events\Api\EventInterface;

/**
 * Class Event
 * @package Extended\Events
 */
abstract class Event Implements \Serializable, EventInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'event' => [
                'name' => $this->getName()
            ]
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $buffer = unserialize($serialized);
        $this->name = $buffer['event']['name'];
    }

    /**
     * The actual method of the event.
     *
     * @return mixed
     */
    abstract public function execute();
}