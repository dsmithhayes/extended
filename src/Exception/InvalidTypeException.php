<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Exception;

use Exception;

/**
 * When extending abstract and generic classes, or implementing interfacing and
 * you wish to refine the parameters by type, you should throw this when a
 * failed assertion or type check happens.
 *
 * Class InvalidTypeException
 * @package Extended\Exception
 */
class InvalidTypeException extends Exception
{
    /**
     * @var string
     */
    public $expected = '';

    /**
     * @var string
     */
    public $actual = '';

    /**
     * @param $expected
     * @param $actual
     * @return string
     */
    public function buildMessage(string $expected, string $actual): string
    {
        $this->expected = $expected;
        $this->actual = $actual;
        return "Invalid type: '{$actual}', expected: '{$expected}'";
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->buildMessage($this->expected, $this->actual);
    }
}