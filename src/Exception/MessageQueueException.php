<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Exception;

use Exception;

/**
 * Class MessageQueueException
 * @package Extended\Exception
 */
class MessageQueueException extends Exception { }