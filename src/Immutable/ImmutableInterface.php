<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Immutable;

use Extended\Mutable\GetInterface;

/**
 * When something is immutable, it means that its values cannot be altered. PHP
 * has a pesky problem of not being very immutable, so things can change
 * without you even realizing it. The idea of this interface, and the subsequent
 * class, is that the values cannot be altered.
 *
 * Interface ImmutableInterface
 * @package Extended\Immutable
 */
interface ImmutableInterface extends GetInterface { }
