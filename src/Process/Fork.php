<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Process;

use Extended\Exception\ProcessException;
use Extended\Stream\Buffer;
use DomainException;

/**
 * Class Fork
 * @package Extended\Process
 */
class Fork
{
    /**
     * @var int The PID of the forked process
     */
    protected $pid;

    /**
     * @var Buffer Active buffer of all the children's STDOUT
     */
    protected static $stdoutBuffer;

    /**
     * @param string $buffer Pre-existing STDOUT buffer
     * @throws DomainException If the PCNTL extension is not enabled, don't
     *                         allow the the class to be constructed.
     */
    public function __construct($buffer = '')
    {
        if (!extension_loaded('pcntl')) {
            throw new DomainException("Module 'pcntl' not enabled.");
        }

        self::$stdoutBuffer = new class($buffer) extends Buffer {
            public function append($b)
            {
                $this->buffer .= $b;
                return $this;
            }
        };
    }

    /**
     * @param Runnable $child
     *      A runnable process to fork
     * @return $this
     * @throws ProcessException
     */
    public function fork(Runnable $child)
    {
        $this->pid = pcntl_fork();

        if ($this->pid == -1) {
            $error = pcntl_strerror(pcntl_get_last_error());
            throw new ProcessException('Unable to fork the process: ' . $error);
        } elseif ($this->pid) {
            return pcntl_wait($this->pid);
        }

        $stdout = $child->run();
        self::$stdoutBuffer->append($stdout);
        return $this;
    }

    /**
     * @return string
     *      The output of all the children processes.
     */
    public function getBuffer()
    {
        return self::$stdoutBuffer->output();
    }

    /**
     * Clears all of the data in the current buffer.
     */
    public function clearBuffer()
    {
        self::$stdoutBuffer->clear();
    }
}
