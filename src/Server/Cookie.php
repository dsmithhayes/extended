<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Server;

use Serializable;
use InvalidArgumentException;

/**
 * Wrapper for cookie functions.
 *
 * Class Cookie
 * @package Dsh\Blog\Core
 */
class Cookie implements Serializable
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $value = "";

    /**
     * @var int
     */
    private $expire = 0;

    /**
     * @var string
     */
    private $path = "";

    /**
     * @var string
     */
    private $domain = "";

    /**
     * @var bool
     */
    private $secure = false;

    /**
     * @var bool
     */
    private $httpOnly = false;

    /**
     * @var bool
     */
    private $exists = false;

    /**
     * Cookie constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        if (isset($_COOKIE[$name])) {
            $this->setValue($_COOKIE[$name]);
            $this->exists = true;
        }

        $this->name = $name;
    }

    /**
     * @param $property
     * @return null
     */
    public function __get($property)
    {
        if (property_exists(self::class, $property)) {
            return $this->{$property};
        }

        throw new InvalidArgumentException("Property '{$property}' does not exist.");
    }

    /**
     * @return bool
     */
    public function set(): bool
    {
        return ($this->exists = setcookie(
            $this->name,
            $this->value,
            $this->expire,
            $this->path,
            $this->domain,
            $this->secure,
            $this->httpOnly
        ));
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize([
            'name'      => $this->name,
            'value'     => $this->value,
            'expire'    => $this->expire,
            'path'      => $this->path,
            'domain'    => $this->domain,
            'secure'    => $this->secure,
            'httpOnly'  => $this->httpOnly
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $buffer = unserialize($serialized);

        foreach ($buffer as $property => $value) {
            $this->{$property} = $value;
        }
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     *
     *
     * @param int $seconds
     * @return $this
     */
    public function setExpire(int $seconds)
    {
        $this->expire = time() + $seconds;
        return $this;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setPath(string $path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain(string $domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @param bool $secure
     * @return $this
     */
    public function setSecure(bool $secure)
    {
        $this->secure = $secure;
        return $this;
    }

    /**
     * @param bool $httpOnly
     * @return $this
     */
    public function setHttpOnly(bool $httpOnly)
    {
        $this->httpOnly = $httpOnly;
        return $this;
    }

    /**
     * @return bool
     */
    public function exists(): bool
    {
        return $this->exists;
    }

    /**
     * @return bool
     */
    public function persist(): bool
    {
        return setcookie(
            $this->name,
            $this->value,
            $this->expire,
            $this->path,
            $this->domain,
            $this->secure,
            $this->httpOnly
        );
    }

    /**
     * @param string $name
     * @return Cookie
     */
    public static function factory(string $name): Cookie
    {
        return new self($name);
    }
}