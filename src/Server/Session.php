<?php

/**
 * @author Dave Smith-Hayes <me@davesmithayes.com>
 */

namespace Extended\Server;

use SessionHandlerInterface;
use Serializable;
use InvalidArgumentException;

/**
 * Wraps the session functions.
 *
 * Class Session
 * @package Dsh\Blog\Core
 */
class Session implements Serializable
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Session constructor.
     * @param array $opts
     * @param bool $autoStart
     * @param string|null $id
     */
    public function __construct(
        array  $opts      = [],
        bool   $autoStart = false,
        string $id        = null
    ) {
        $this->options = $opts;

        if ($id) {
            $this->setId($id);
        }

        if ($autoStart) {
            $this->start();
        }
    }

    /**
     * Destroy the session when the object is destroyed, coupling sessions to
     * the object.
     */
    public function __destruct()
    {
        if ($this->isActive()) {
            session_destroy();
        }
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name)
    {
        if (!isset($_SESSION[$name])) {
            throw new InvalidArgumentException("Session key does not exist: '{$name}'");
        }

        return $_SESSION[$name];
    }

    /**
     * `set` takes the liberty of serializing or casting-as-a-string to anything
     * you give it as a value.
     *
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function set(string $name, $value)
    {
        if (($value instanceof Serializable) || is_array($value)) {
            $buffer = serialize($value);
        } else {
            $buffer = (string) $value;
        }

        $_SESSION[$name] = $buffer;
        return $this;
    }

    /**
     * @return string
     */
    public function serialize()
    {
        $this->destroy();
        return serialize($_SESSION);
    }

    /**
     * @param string $data
     */
    public function unserialize($data)
    {
        $this->start();
        foreach (unserialize($data) as $key => $value) {
            $_SESSION[$key] = $value;
        }
    }

    /**
     * @return bool
     */
    public function start(): bool
    {
        return session_start($this->options);
    }

    /**
     * @return bool
     */
    public function destroy(): bool
    {
        return session_destroy();
    }

    /**
     * @return $this
     */
    public function abort()
    {
        session_abort();
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        session_id($id);
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return session_id();
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return (bool) (session_status() & PHP_SESSION_ACTIVE);
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return (bool) (session_status() & PHP_SESSION_DISABLED);
    }

    /**
     * @return bool
     */
    public function isNone(): bool
    {
        return (bool) (session_status() & PHP_SESSION_NONE);
    }

    /**
     * @param SessionHandlerInterface $handler
     * @param bool $shutdown
     * @return $this
     */
    public function setSaveHandler(SessionHandlerInterface $handler, bool $shutdown = true)
    {
        session_set_save_handler($handler, $shutdown);
        return $this;
    }
}