<?php

namespace Extended\Stream\Buffer;

use Extended\Stream\Buffer;

/**
 * Class TextBuffer
 * @package Extended\Stream\Buffer
 */
class TextBuffer extends Buffer
{
    /**
     * @param string $buffer
     * @return $this
     */
    public function setText(string $buffer)
    {
        $this->input($buffer);
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->output();
    }

    /**
     * @param string $buffer
     * @return Buffer|void
     */
    public function input($buffer)
    {
        if (!is_string($buffer)) {
            throw new \InvalidArgumentException("Buffer is not type 'string'");
        }

        parent::input($buffer);
    }
}