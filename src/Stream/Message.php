<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Stream;

/**
 * Class Message
 * @package Extended\Stream
 */
abstract class Message
{
    /**
     * @var string
     */
    protected $topic;

    /**
     * @var string
     */
    protected $body;

    /**
     * @var int
     */
    protected $inputTime;

    /**
     * Message constructor.
     * @param string $topic
     * @param string $body
     */
    public function __construct(string $topic, string $body)
    {
        $this->topic = $topic;
        $this->body = $body;
        $this->inputTime = time();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->body;
    }

    /**
     * @return string
     */
    public function getTopic(): string
    {
        return $this->topic;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return int
     */
    public function getInputTime(): int
    {
        return $this->inputTime;
    }
}