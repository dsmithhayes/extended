<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Stream;

use Extended\Exception\InvalidTypeException;
use Extended\Stream\Message;
use Extended\Collections\Queue\BasicQueue;
use Extended\Exception\MessageQueueException;

/**
 * Class MessageQueue
 * @package Extended\Collections\Queue
 */
class MessageQueue extends BasicQueue
{
    /**
     * @var Message[]
     */
    protected $messages;

    /**
     * MessageQueue constructor.
     * @param array $q
     * @throws InvalidTypeException
     */
    public function __construct(array $q = [])
    {
        if (!empty($q)) {
            foreach ($q as $message) {
                if (!$this->isMessage($message)) {
                    $invalidTypeException = new InvalidTypeException();
                    $invalidTypeException->expected = Message::class;
                    $invalidTypeException->actual = gettype($message);
                    throw $invalidTypeException;
                }
            }
        }

        parent::__construct($q);
    }

    /**
     * @param $value
     * @return bool
     */
    protected function isMessage($value): bool
    {
        return $value instanceof Message;
    }

    /**
     * @param mixed $item
     * @return $this
     * @throws MessageQueueException
     */
    public function enqueue($item)
    {
        if (!$this->isMessage($item)) {
            $className = Message::class;
            throw new MessageQueueException("{$item} not of instance {$className}");
        }

        parent::enqueue($item);
        return $this;
    }
}