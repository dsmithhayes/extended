<?php

namespace Extended\Tests\Collections;

use Extended\Collections\BasicArray;
use PHPUnit\Framework\TestCase;

class BasicArrayTests extends TestCase
{
    protected function _getIntergerArrayBuffer(): array
    {
        return [ 0, 1, 2, 3, 4, 5, 6, ];
    }

    public function testBasicArray()
    {
        $arr = new BasicArray($this->_getIntergerArrayBuffer());
        $this->assertEquals(0, $arr[0]);

        // $arr is a first class object that has array properties
        $this->assertIsNotArray($arr);

        // casting will make the object an array for explicit type checks
        $this->assertIsArray((array) $arr);

        $callback = function ($x) {
            return $x * 2;
        };

        $mappedArr = $arr->map($callback);
        $this->assertEquals(0, $mappedArr[0]);
        $this->assertEquals(2, $mappedArr[1]);

    }
}