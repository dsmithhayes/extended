<?php

namespace Extended\Tests\Collections;

use PHPUnit\Framework\TestCase;

/**
 * Base test class with reproducible seed data for tests.
 *
 * Class CollectionsTestCase
 * @package Extended\Tests\Collections
 */
class CollectionsTestCase extends TestCase
{
    /**
     * This array will always be the following:
     * [
     *      0 => 1,
     *      1 => 2,
     *      2 => 3,
     *      3 => 4
     * ]
     *
     * @return array
     */
    protected function _createNumericalArrayBuffer(): array
    {
        return [ 1, 2, 3, 4 ];
    }

    /**
     * Used to evade warnings
     */
    public function testMethodPlaceholder()
    {
        $this->assertEquals(1,1);
    }
}