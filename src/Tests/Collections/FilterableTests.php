<?php

namespace Extended\Tests\Collections;

use Extended\Collections\BasicArray;
use Extended\Collections\Filterable;
use Extended\Tests\Collections\CollectionsTestCase;

/**
 * Class FilterableTests
 * @package Extended\Tests\Collections
 */
class FilterableTests extends CollectionsTestCase
{
    public function testFilterable()
    {
        $arr = new BasicArray([ 1, 2, 3, 4 ]);
        $arr = $arr->filter(function ($value) {
            return ($value % 2) == 0;
        });

        // filter will maintain the original key name.
        $this->assertEquals(2, $arr[1]);
        $this->assertArrayNotHasKey(0, $arr);
    }
}