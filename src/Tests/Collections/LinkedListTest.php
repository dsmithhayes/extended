<?php

namespace Extended\Tests\Collections;

use Extended\Collections\LinkedList;
use Extended\Tests\Collections\CollectionsTestCase;

/**
 * Class LinkedListTest
 * @package Extended\Tests\Collections
 */
class LinkedListTest extends CollectionsTestCase
{
    public function testBasicIO()
    {
        $arrayBuffer = $this->_createNumericalArrayBuffer();
        $list = new LinkedList($arrayBuffer);

        $this->assertEquals(1, $list->get(0));

        foreach ($list as $item) {
            $this->assertEquals(1, $item);
            break;
        }

        $list->add(5);
        $this->assertEquals(5, $list->getLast());
    }
}