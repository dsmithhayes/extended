<?php

namespace Extended\Tests\Collections\Queues;

use Extended\Collections\Queue\BasicQueue;
use Extended\Collections\Queue\BasicQueueFactory;
use Extended\Tests\Collections\CollectionsTestCase;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

/**
 * Class QueueTests
 * @package Extended\Tests\Collections\Queues
 */
class QueueTests extends CollectionsTestCase
{
    /**
     * Test all of the implemented BasicQueue methods.
     */
    public function testBasicQueueIO()
    {
        $numericalBuffer = $this->_createNumericalArrayBuffer();
        /** @var BasicQueue $queue */
        $queue = BasicQueueFactory::create($numericalBuffer);

        $output = $queue->dequeue();
        $this->assertEquals(1, $output);

        $output = $queue->dequeue();
        $this->assertEquals(2, $output);

        foreach ($queue as $item) {
            $this->assertEquals(3, $item);
            break;
        }

        $output = $queue->dequeue();
        $this->assertEquals(4, $output);

        try {
            $output = $queue->dequeue();
        } catch (\Exception $e) {
            $actualClassName = get_class($e);
            $expectedClassName = InvalidArgumentException::class;

            $this->assertEquals($actualClassName, $expectedClassName);
        }
    }
}