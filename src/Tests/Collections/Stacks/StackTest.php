<?php

namespace Extended\Tests\Stacks;

use Extended\Collections\Stack\BasicStackFactory;
use Extended\Tests\Collections\CollectionsTestCase;
use InvalidArgumentException;

/**
 * Class StackTest
 * @package Extended\Tests\Stacks
 */
class StackTest extends CollectionsTestCase
{
    /**
     *
     */
    public function testBasicStackIO()
    {
        $stackBuffer = $this->_createNumericalArrayBuffer();
        $stack = BasicStackFactory::create($stackBuffer);

        $output = $stack->pop();
        $this->assertEquals(4, $output);

        foreach ($stack as $item) {
            $this->assertEquals(3, $item);
            break;
        }

        $output = $stack->pop();
        $this->assertEquals(2, $output);

        $output = $stack->pop();
        $this->assertEquals(1, $output);

        try {
            $output = $stack->pop();
        } catch (\Exception $e) {
            $actualClassName = get_class($e);
            $expectedClassName = InvalidArgumentException::class;

            $this->assertEquals($actualClassName, $expectedClassName);
        }

        $stack->push(0);
        $stack->push(1);
        $this->assertEquals(1, $stack->pop());
        $stack->push(1);
        $this->assertEquals(1, $stack->pop());
        $this->assertEquals(0, $stack->pop());
    }
}