<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Traits;

use Extended\Traits\MapTrait;

/**
 * Trait DeepCopyTrait
 * @package Extended\Traits
 */
trait DeepCopyTrait
{
    /**
     * @param mixed $value
     *      The value to copy
     * @return mixed
     *      The deep copied values
     */
    private function deepCopy($value)
    {
        if (is_array($value)) {
            foreach ($value as &$val) {
                $val = $this->deepCopy($val);
            }

            return $value;
        }

        if (is_object($value)) {
            return clone $value;
        }

        /** @var mixed $buffer Attach the original value to a new variable */
        $buffer = $value;
        return $buffer;
    }
}
