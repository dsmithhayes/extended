<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Traits;

/**
 * Trait MapTrait
 * @package Extended\Traits
 */
trait MapTrait
{
    /**
     * @param array $arr
     * @param callable $callback
     * @return array
     */
    public function map(array $arr, callable $callback): array
    {
        foreach ($arr as $key => $value) {
            $arr[$key] = $callback($value);
        }

        return $arr;
    }

    /**
     * @param array $arr
     * @param callable $callback
     * @return array
     */
    public function deepMap(array $arr, callable $callback): array
    {
        foreach ($arr as $key => $value) {
            if (is_array($value)) {
                $arr[$key] = $this->deepMap($value, $callback);
            } else {
                $arr[$key] = $callback($value);
            }
        }

        return $arr;
    }

    /**
     * @param array $arr
     * @param callable $callback
     * @return \Generator
     */
    public function mapGenerator(array $arr, callable $callback)
    {
        foreach ($arr as $val) {
            yield $callback($val);
        }
    }

}
