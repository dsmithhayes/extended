<?php

/**
 * @author Dave Smith-Hayes <me@davesmithhayes.com>
 */

namespace Extended\Traits;

/**
 * Trait MutableTrait
 * @package Extended\Traits
 */
trait MutableTrait
{
    /**
     * @param string $name
     *      The name of the property to set
     * @param mixed $value
     *      The value for the property to set
     */
    public function __set(string $name, $value)
    {
        $this->set($name, $value);
    }

    /**
     * @param string $name
     *      The name of the property
     * @param mixed $value
     *      The value to set for the property within the object
     * @return $this
     */
    public function set($name, $value)
    {
        $this->{$name} = $value;
        return $this;
    }

    /**
    * @param string $name
    *      The name of the property to get
    * @return mixed
    *      The value of the property
    */
    public function __get(string $name)
    {
        return $this->get($name);
    }

    /**
     * @param string $name
     *      The name of the property
     * @return mixed
     *      The value of the property
     */
    public function get($name)
    {
        return $this->{$name};
    }
}
