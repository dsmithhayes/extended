<?php

use Extended\File\File;

class FileTestCase extends PHPUnit_Framework_TestCase
{
    protected static $path;

    public function setupTestFile()
    {
        static::$path = dirname(__FILE__) . '/assets/test_file.txt';
    }

    public function testOpenFile()
    {
        static::setupTestFile();
        $file = new File(static::$path);

        $expected = 'test';
        $file->input($expected);

        $fileOutput = trim($file->output());

        $this->assertEquals($expected, $fileOutput);
        $this->assertInstanceOf('\Extended\File\File', $file->save());

        $file2 = new File(static::$path, 'r');
        $fileOutput = trim($file2->output());
        $this->assertEquals($expected, $fileOutput);
    }
}
