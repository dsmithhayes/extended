<?php

use PHPUnit_Framework_TestCase as PHPUnitTestCase;
use Extended\Stream\Message;
use Extended\Stream\MessageQueue;
use Extended\Exception\MessageQueueException;
use Extended\Exception\InvalidTypeException;

class MessageQueueTestCase extends PHPUnitTestCase
{
    /**
     * @param string $title
     * @param string $message
     * @return Message
     */
    protected function buildMessage(string $title, string $message): Message
    {
        return new class($title, $message) extends Message {};
    }

    public function testBuild()
    {
        $messageQueue = new MessageQueue();

        for ($i = 0; $i < 10; $i++) {
            $title = "Title #{$i}";
            $message = "Message #{$i}";
            $messageQueue->enqueue($this->buildMessage($title, $message));
        }

        $firstMessage = $messageQueue->dequeue();
        $this->assertEquals("Title #0", $firstMessage->getTitle());

        // tests __toString
        $this->assertEquals("Message #0", (string) $firstMessage);
    }

    /**
     * @expectedException \Extended\Exception\InvalidTypeException
     */
    public function testConstructorException()
    {
        $messageQueue = new MessageQueue(['Hello!']);
    }

    /**
     * @expectedException \Extended\Exception\MessageQueueException
     */
    public function testEnqueueException()
    {
        $messageQueue = new MessageQueue();
        $messageQueue->enqueue('Hello.');
    }

    public function testInvalidTypeException()
    {
        try {
            $messageQueue = new MessageQueue(['Hello']);
        } catch (InvalidTypeException $ite) {
            $this->assertEquals('Extended\Stream\Message', $ite->expected);
        }
    }
}