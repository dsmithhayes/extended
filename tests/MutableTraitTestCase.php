<?php

use PHPUnit_Framework_TestCase as PHPUnitTestCase;
use Extended\Traits\MutableTrait;
use Extended\Mutable\GetInterface;
use Extended\Mutable\SetInterface;

class MutableTraitTestCase extends PHPUnitTestCase
{
    public function testMutableTraitWithInterface()
    {
        $m = new class implements GetInterface, SetInterface {
            use MutableTrait;
        };

        $m->set('x', 1);
        $this->assertEquals(1, $m->x);

        $m->y = 2;
        $this->assertEquals(2, $m->get('y'));
    }

    public function testMutableTraitOverRide()
    {
        $m = new class implements GetInterface, SetInterface {
            use MutableTrait;

            private $x = 6;

            public final function __set($name, $value)
            {
                return;
            }
        };

        $this->assertEquals(6, $m->get('x'));

        $m->set('x', 5);
        $this->assertEquals(5, $m->x);

        $m->x = 6;
        $this->assertNotEquals(6, $m->x);
    }
}